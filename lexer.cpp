#include <algorithm>

#include "lexer.hpp"

std::vector<char> known_digits {
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
    '0'
};

std::vector<char> known_letters {
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 
    'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
    'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D',
    'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 
    'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
    'Y', 'Z', '+', '-', '*', '/'
};

Lexer::Lexer(std::string input) : m_input(input) { };

void Lexer::append(Token* token) {
    this->m_tokens.push_back(token);
}

std::vector<Token*> Lexer::tokens() {
    return this->m_tokens;
}

void Lexer::advance() {
    this->m_cursor++;
}

char Lexer::current_character() {
    return this->m_input.at(this->m_cursor);
}

bool Lexer::is_eligible_to_advance() {
    return this->m_cursor < this->m_input.size();
}

bool Lexer::current_character_is_a_digit() {
    return std::find(known_digits.begin(), known_digits.end(),
                     this->current_character()) != known_digits.end();
}

bool Lexer::current_character_is_a_letter() {
    return std::find(known_letters.begin(), known_letters.end(),
                     this->current_character()) != known_letters.end();
}

bool Lexer::current_character_is_opening_paren() {
    return this->current_character() == '(';
}

bool Lexer::current_character_is_closing_paren() {
    return this->current_character() == ')';
}

std::vector<Token*> Lexer::lex(std::string input) {
    Lexer* self = new Lexer(input);
    while (self->is_eligible_to_advance()) {
        if (self->current_character_is_opening_paren()) {
            self->append(new Token(std::string(1, self->current_character()), TokenType::OpeningParen));
            self->advance();
        } else if (self->current_character_is_closing_paren()) {
            self->append(new Token(std::string(1, self->current_character()), TokenType::ClosingParen));
            self->advance();
        } else if (self->current_character_is_a_letter()) {
            std::string value = std::string(1, self->current_character());
            while (self->is_eligible_to_advance()) {
                self->advance();
                if (!(self->current_character_is_a_letter() || self->current_character_is_a_digit())) {
                    break;
                } else {
                    value.append(std::string(1, self->current_character()));
                }
            }
            self->append(new Token(value, TokenType::Identifier));
        } else if (self->current_character_is_a_digit()) {
            std::string value = std::string(1, self->current_character());
            while (self->is_eligible_to_advance()) {
                self->advance();
                if (!(self->current_character_is_a_letter() || self->current_character_is_a_digit())) {
                    break;
                } else {
                    value.append(std::string(1, self->current_character()));
                }
            }
            self->append(new Token(value, TokenType::Number));
        } else {
            self->advance();
        }
    }

    return self->tokens(); // return all the lexed tokens and don't take care of deleting Lexer instance
}