#pragma once

#include "expression.hpp"

struct ReadResult {
    Expression* expression;
    int cursor;
};

ReadResult* read(std::vector<Token*> tokens, int cursor = 0);