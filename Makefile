tests: toylisp
	./tests.sh

BIN += toylisp

OBJECTS += vm.o \
           main.o \
           core.o \
           token.o \
           lexer.o \
           reader.o \
           literal.o \
           expression.o		   

CXXFLAGS += -std=c++17 -fPIC

ARTIFACTS += $(OBJECTS) $(BIN)

fresh: clean $(BIN)

clean:
	rm $(ARTIFACTS) 2>/dev/null || true

$(BIN): $(OBJECTS)
	$(CXX) \
        $(CXXFLAGS) $(OBJECTS) -o $(BIN)

vm.o: vm.cpp
	$(CXX) $(CXXFLAGS) -c vm.cpp -o vm.o

main.o: main.cpp
	$(CXX) $(CXXFLAGS) -c main.cpp -o main.o

core.o: core.cpp core.hpp
	$(CXX) $(CXXFLAGS) -c core.cpp -o core.o

token.o: token.cpp token.hpp
	$(CXX) $(CXXFLAGS) -c token.cpp -o token.o

lexer.o: lexer.cpp lexer.hpp
	$(CXX) $(CXXFLAGS) -c lexer.cpp -o lexer.o

reader.o: reader.cpp reader.hpp
	$(CXX) $(CXXFLAGS) -c reader.cpp -o reader.o

literal.o: literal.cpp literal.hpp
	$(CXX) $(CXXFLAGS) -c literal.cpp -o literal.o

expression.o: expression.cpp expression.hpp
	$(CXX) $(CXXFLAGS) -c expression.cpp -o expression.o
