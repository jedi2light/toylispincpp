#include <iostream>

#include "vm.hpp"
#include "core.hpp"
#include "lexer.hpp"
#include "reader.hpp"

int main(int argc, char* argv[]) {
    if (argc < 2) {
        std::cerr << "No source code file" << std::endl;
        return 1;
    }

    char* source_file = argv[0];
    std::string source_code = CoreBackend::slurp(source_file);

    Expression* root = read(Lexer::lex(source_code))->expression;

    VM* vm = new VM();
    for (auto& node : root->nodes()) {
        ByteCode* code = ByteCode::generate(node);
        vm->execute(code);
    }

    std::cout << vm->last_result_as_a_formated_string() << std::endl;

    return 0;
}