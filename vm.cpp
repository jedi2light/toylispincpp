#include "vm.hpp"

NillValue::NillValue() {}

std::string NillValue::to_string() {
    return "nil";
}

NumberValue::NumberValue(float number_as_float) :
    m_number_as_float(number_as_float) {}

std::string NumberValue::to_string() {
    return std::to_string(this->m_number_as_float);
}

FunctionValue::FunctionValue(VMValue* function_pointer) :
    m_function_pointer(function_pointer) {}    

std::string FunctionValue::to_string() {
    return "<An anonymous(?) ToyLISP VMFunctionValue>";
}

BCodeInstruction::BCodeInstruction() : 
    m_instruction_op_type(BCodeInstructionOpType::NOOP)
    {}

ByteCode::ByteCode() {}

void ByteCode::append_byte_code_instruction(
    BCodeInstruction* byte_code_instruction) {
    this->m_instructions.push_back(
        byte_code_instruction
    );
}

std::vector<BCodeInstruction*> ByteCode::instructions() {
    return this->m_instructions;
}

ByteCode* ByteCode::generate (ExpressionNodeType& node) {
    // todo: walk through nodes and append an instruction
    ByteCode* instance = new ByteCode();
    instance->append_byte_code_instruction(
        new BCodeInstruction()  // default one is: 'NOOP'
    );
    return instance;
}

// todo: initialize registers and so on
VM::VM() : m_last_result(new NillValue()) {}

void VM::execute(ByteCode* code) {
    // todo: execute byte code instruction by instruction
}

std::string VM::last_result_as_a_formated_string() {
    return this->m_last_result->to_string();
}