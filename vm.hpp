#pragma once

#include <vector>

#include "expression.hpp"

enum VMValueType {
    NillValueType, 
    NumberValueType, 
    FunctionValueType
};

class VMValue {
public:
    virtual std::string to_string() = 0;
};

class NillValue : public VMValue {
public:
    std::string to_string();
    NillValue();
};

class NumberValue : public VMValue {
private:
    float m_number_as_float;
public:
    std::string to_string();
    NumberValue(float number_as_float);
};

class FunctionValue : public VMValue {
private:
    VMValue* m_function_pointer;
public:
    std::string to_string();
    FunctionValue(VMValue* m_function_pointer);
};

enum BCodeInstructionOpType {
    NOOP, SET, GET, INVOKE
};

class BCodeInstruction {
private:
    BCodeInstructionOpType m_instruction_op_type;
public:
    BCodeInstruction();
};

class ByteCode {
private:
    std::vector<BCodeInstruction*> m_instructions;
public:
    ByteCode();
    void append_byte_code_instruction(
        BCodeInstruction* byte_code_instruction
    );
    std::vector<BCodeInstruction*> instructions();
    static ByteCode* generate(ExpressionNodeType&);
};

class VM {
private:
    VMValue* m_last_result;
public:
    VM();
    void execute(ByteCode* code);
    std::string last_result_as_a_formated_string();
};