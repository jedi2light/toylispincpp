#include "reader.hpp"

ReadResult* read(std::vector<Token*> tokens, int cursor) {
    std::vector<ExpressionNodeType> collected_nodes;

    while (cursor < tokens.size() - 1) {
        Token* current_token = tokens.at(cursor);

        if (current_token->is_opening_paren()) {
            ReadResult* result = read(tokens, cursor + 1);
            cursor = result->cursor;
            if (result->expression != nullptr)
                collected_nodes.push_back(*result->expression);
        } else if (current_token->is_closing_paren()) break;
        else collected_nodes.push_back(Literal(current_token));
        cursor++;
    }

    return new ReadResult{ new Expression(collected_nodes), cursor };
}