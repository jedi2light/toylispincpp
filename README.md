# Toy LISP in C++

## Aim

Aim of this project is to create a toy LISP interpreter in C++

## Steps

`[+]` Lexer  
`[+]` Parser/Reader  
`[/]` Interpretation  

## Usage

On GNU/Linux: `$ make fresh && make tests`. This one will ensure you have the latest version compiled (not fetched!) and run the tests using [underlying `bash` script](./tests.sh). All the tests are LISP files and located in [`tests/`](./tests) directory.
