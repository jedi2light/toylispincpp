#pragma once

#include <string>
#include <vector>

#include "token.hpp"

class Lexer {
private:
    std::vector<Token*> m_tokens;
    std::string m_input;
    int m_cursor { 0 };
public:
    Lexer(std::string input);
    void append(Token* token);
    std::vector<Token*> tokens();
    void advance();
    char current_character();
    bool is_eligible_to_advance();
    bool current_character_is_a_digit();
    bool current_character_is_a_letter();
    bool current_character_is_opening_paren();
    bool current_character_is_closing_paren();
    static std::vector<Token*> lex(std::string input);
};