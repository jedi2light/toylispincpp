#include "token.hpp"

std::string Token::value() {
    return this->m_value;
}

TokenType Token::type() {
    return this->m_type;
}

bool Token::is_opening_paren() {
    return this->m_type == TokenType::OpeningParen;
}

bool Token::is_closing_paren() {
    return this->m_type == TokenType::ClosingParen;
}

Token::Token(std::string value, TokenType type) : m_value(value),
                                                  m_type(type) {};
