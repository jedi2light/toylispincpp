#pragma once

#include <vector>
#include <variant>

#include "literal.hpp"

class Expression;

typedef std::variant<Literal, Expression>
        ExpressionNodeType;

class Expression {
private:
    std::vector<ExpressionNodeType> m_nodes;
public:
    Expression(std::vector<ExpressionNodeType> nodes);
    std::vector<ExpressionNodeType> nodes();
    int dump(int indentation = 0);
};