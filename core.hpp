#pragma once

#include <string>

class CoreBackend {
public:
    static std::string slurp(std::string);
};