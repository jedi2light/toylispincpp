#pragma once

#include <string>

#include "token.hpp"

class Literal {
private:
    Token* m_token;
public:
    Literal(Token* token);
    Token* token();
    int dump(int indentation = 0);
};