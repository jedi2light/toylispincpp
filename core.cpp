#include <fstream>
#include <sstream>

#include "core.hpp"

std::string CoreBackend::slurp(std::string path) {
    std::ifstream reader(path);
    std::stringstream buffer;
    buffer << reader.rdbuf();
    return buffer.str();
}