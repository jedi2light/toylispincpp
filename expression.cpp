#include <iostream>

#include "expression.hpp"

Expression::Expression(std::vector<ExpressionNodeType> nodes) : m_nodes(nodes) {};

std::vector<ExpressionNodeType> Expression::nodes() {
    return this->m_nodes;    
}

int Expression::dump(int indentation) {
    std::cout << (indentation > 0 ? std::string(indentation, ' ') : "") << "(" << std::endl;
    int new_indentation = 0;
    for (auto& node : this->nodes())
        if (Literal* maybe_literal = std::get_if<Literal>(&node))
            new_indentation = maybe_literal->dump(indentation + 1);
        else if (Expression* maybe_expression = std::get_if<Expression>(&node)) 
            new_indentation = maybe_expression->dump(indentation + 1);
        else
            std::cout << "Expression::dump(): Unable to print node for unknown reason" << std::endl;
    std::cout << (new_indentation > 0 ? std::string(new_indentation, ' ') : "") << ")" << std::endl;
    return new_indentation;
}