#pragma once

#include <string>

enum TokenType {
    OpeningParen,
    ClosingParen,
    AnythingElse,
    Identifier, Number
};

class Token {
private:
    std::string m_value;
    TokenType m_type;
public:
    std::string value();
    TokenType type();
    bool is_opening_paren();
    bool is_closing_paren();
    Token(std::string value, TokenType type);
};
