#include <iostream>

#include "literal.hpp"

Literal::Literal(Token* token) : m_token(token) {};

Token* Literal::token() {
    return this->m_token;
}

int Literal::dump(int indentation) {
    std::cout << (indentation > 0 ? std::string(indentation, ' ') : "") << this->token()->value() << std::endl;
    return indentation;
}